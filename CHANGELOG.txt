etracker 8.x-3.0-beta3, 2022-09-30
------------------------------------
#3305277 by sunlix, Project Update Bot: Automated Drupal 10 compatibility fixes
#3301609 by sunlix, Anybody: JS has hard dependency of cookies/cookiesjsr causes double load of cookiesjsr-preloader
#3301610 by sunlix: Remove unused cookies_etracker.js
#3249837 by sunlix: Fix coding standards
#3301571 by sunlix: Drupal 10 compatibility
#3220360 by Anybody, sunlix, Grevil: COOKiES Integration
#3284618 by Anybody: etracker.js: "Uncaught ReferenceError: drupalSettings is not defined" (Missing core/drupalSettings library dependency)
#3250661 by Anybody: Notice: Array to string conversion in _etracker_set_default_variables()
#3241158 by Grevil, sunlix, Anybody: Add functional tests for the main etracker module
#3248231 by sunlix: Improve AdminSettingsForm
#3245186 by Anybody, sunlix, Grevil: Paths exlusions have wrong notation, should start with /
#3243371 by Grevil, sunlix: Settings: Tracking a specific role still tracks that selected role
#3243378 by Grevil, Anybody: Remove old test_dependencies
#3225085 by sunlix: Make default Event-Tracking configureable

etracker 8.x-3.0-beta2, 2021-09-21
------------------------------------
#3238216 by sunlix, Anybody: Tracking snippet seems to require id="_etLoader" to track correctly
#3227817 by sunlix, Anybody: Uncaught ReferenceError: ET_Event is not defined
#3238258 by sunlix: Support script CSP
#3227910 by sunlix, design.er, Anybody: Rewrite etracker.js in vanilla JavaScript and remove jQuery dependency

etracker 8.x-3.0-beta1, 2021-07-23
------------------------------------
#3223950 by sunlix: Add TugboatQA
#3147236 by sunlix: Drupal 9 compatibility
#3220360 by sunlix, Anybody: COOKiES Integration
#3218188 by design.er, sunlix: Load tracking code async

etracker 8.x-3.x
--------------------
- Complete rewrite against Drupal 8 API

etracker 7.x-2.x-dev
--------------------
- Complete rewrite against etrackers new API and using rules
