## SUMMARY

This modules integrates web tracking solution from etracker:

Quote from their website: "etracker is a leading provider of solutions and
services for web analysis and optimization of online marketing campaigns.
etracker solutions are designed to meet the needs of your online business
model and are 100% compliant with data privacy requirements."

## DETAILS

Please see detailed description at the modules home page at
https://www.drupal.org/project/etracker

## GETTING STARTED

To install this module just follow instructions that apply to all contrib
modules for Drupal 7. Detailed documentation is available here:
https://www.drupal.org/documentation/install/modules-themes/modules-7

Once installed, goto admin/config/system/etracker and provide your
"Account Key 1" in the section "Tracking code". Save your changes and all
should be working just fine.

## CUSTOMIZATION and API

* see https://www.drupal.org/project/etracker
* see also etracker.api.php

## CONTACT and SUPPORT

### Maintainer
* \>= 8.x-3.x - Sven Schüring (sunlix) - https://www.drupal.org/u/sunlix
* <= 7.x-2.x - Jürgen Haas (jurgenhaas) - https://www.drupal.org/user/168924

### This project has been sponsored by
* KRZN - https://www.krzn.de
* PARAGON Executive Services GmbH - http://www.paragon-es.de
