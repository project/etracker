<?php

/**
 * @file
 * Hooks provided by the eTracker module.
 */

/**
 * Alter the default variables for the eTracker JS snippet.
 *
 * @param array $variables
 *   The eTracker default variables.
 */
function hook_etracker_default_variables_alter(array &$variables) {
}

/**
 * Alter the variables for the eTracker JS snippet.
 *
 * @param array $variables
 *   The eTracker variables.
 */
function hook_etracker_variables_alter(array &$variables) {
}
