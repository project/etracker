/**
 * @file
 * eTracker admin behaviors.
 */

(function ($) {
  /**
   * Provide the summary information for the tracking settings vertical tabs.
   */
  Drupal.behaviors.trackingSettingsSummary = {
    attach() {
      // Make sure this behavior is processed only if drupalSetSummary is defined.
      if (typeof jQuery.fn.drupalSetSummary === 'undefined') {
        return;
      }

      $('#edit-pages').drupalSetSummary(function (context) {
        const selector = 'input[name="etracker_track_path_mode"]:checked';
        const $radio = context.querySelector(selector);
        if ($radio.value === 'all_pages') {
          if (
            !context.querySelector('textarea[name="etracker_track_paths"]')
              .value
          ) {
            return Drupal.t('Not restricted');
          }

          return Drupal.t('All pages with exceptions');
        }

        return Drupal.t('Restricted to certain pages');
      });

      $('#edit-roles').drupalSetSummary(function (context) {
        const vals = [];
        context
          .querySelectorAll('input[type="checkbox"]:checked')
          .forEach(function (node) {
            vals.push(node.labels[0].textContent.trim());
          });
        if (!vals.length) {
          return Drupal.t('Not restricted');
        }
        const selector = 'input[name="etracker_track_roles_mode"]:checked';
        if (context.querySelector(selector).value === 'all_roles') {
          return Drupal.t('Excepted: @roles', { '@roles': vals.join(', ') });
        }

        return vals.join(', ');
      });

      $('#edit-users').drupalSetSummary(function (context) {
        const selector = 'input[name="etracker_track_user"]:checked';
        const $radio = context.querySelector(selector);
        if ($radio.value === 'no_customization') {
          return Drupal.t('Not customizable');
        }
        if ($radio.value === 'tracking_on') {
          return Drupal.t('On by default with opt out');
        }

        return Drupal.t('Off by default with opt in');
      });

      $('#edit-breadcrumb').drupalSetSummary(function (context) {
        let $radio = '';
        const selector =
          'input#edit-etracker-mode-breadcrumb-as-area-breadcrumb-on:checked';
        if (context.querySelector(selector)) {
          $radio = Drupal.t('Show area hierarchy');
        }
        const selectorExcludeHome =
          'input#edit-etracker-mode-breadcrumb-as-area-breadcrumb-exclude-home:checked';
        if (context.querySelector(selectorExcludeHome)) {
          $radio = Drupal.t('Hide home page');
        }
        if (!$radio) {
          return Drupal.t('Not tracked');
        }
        return Drupal.t('@items enabled', { '@items': $radio });
      });

      $('#edit-linktracking').drupalSetSummary(function (context) {
        const vals = [];
        if (context.querySelector('input#edit-track-mailto:checked')) {
          vals.push(Drupal.t('Mailto links'));
        }
        if (context.querySelector('input#edit-track-external:checked')) {
          vals.push(Drupal.t('Outbound links'));
        }
        if (context.querySelector('input#edit-track-download:checked')) {
          vals.push(Drupal.t('Downloads'));
        }
        if (!vals.length) {
          return Drupal.t('Not tracked');
        }
        return Drupal.t('@items enabled', { '@items': vals.join(', ') });
      });

      $('#edit-messagetracking').drupalSetSummary(function (context) {
        const vals = [];
        context
          .querySelectorAll('input[type="checkbox"]:checked')
          .forEach(function (node) {
            vals.push(node.labels[0].textContent.trim());
          });
        if (!vals.length) {
          return Drupal.t('Not tracked');
        }
        return Drupal.t('@items enabled', { '@items': vals.join(', ') });
      });

      $('#edit-privacy').drupalSetSummary(function (context) {
        const vals = [];
        if (context.querySelector('input#edit-data-respect-dnt:checked')) {
          vals.push(Drupal.t('Universal web tracking opt-out enabled'));
        }
        if (context.querySelector('input#edit-data-block-cookies:checked')) {
          vals.push(Drupal.t('Cookies disabled'));
        }
        if (!vals.length) {
          return Drupal.t('No privacy');
        }
        return Drupal.t('@items', { '@items': vals.join(', ') });
      });
    },
  };
})(jQuery);
