/**
 * @file
 * Defines Javascript behaviors for the etracker module.
 */
(function (drupalSettings) {
  // Register event listener only if any setting is enabled.
  if (
    drupalSettings.etracker.track_mailto ||
    drupalSettings.etracker.track_download ||
    drupalSettings.etracker.track_external
  ) {
    // Attach mousedown, keyup, touchstart events to document only
    ['mousedown', 'keyup', 'touchstart'].forEach(function (e) {
      document.querySelector('body').addEventListener(e, function (event) {
        if (event.target.tagName === 'A' || event.target.tagName === 'AREA') {
          // Mailto link clicked.
          if (
            drupalSettings.etracker.track_mailto &&
            event.target.href.startsWith('mailto:')
          ) {
            _etracker.sendEvent(
              new et_ClickEvent(
                (`E-Mail:%20${event.target.href.substring(7)}`, 'E-Mail'),
              ),
            );
          }
          // Download link clicked.
          else if (
            drupalSettings.etracker.track_download &&
            new RegExp(
              `\\.(${drupalSettings.etracker.track_download_extensions
                .split(' ')
                .join('|')})(\\?|$)`,
            ).test(event.target.href)
          ) {
            _etracker.sendEvent(
              new et_UserDefinedEvent(event.target.href, 'Download'),
            );
          }
          // External link clicked.
          else if (
            drupalSettings.etracker.track_external &&
            event.target.host !== window.location.host
          ) {
            _etracker.sendEvent(
              new et_UserDefinedEvent(
                `External link:%20${event.target.href}`,
                '',
              ),
            );
          }
        }
      });
    });
  }

  // Trigger events for each message being displayed.
  if (drupalSettings.etracker.messages.length > 0) {
    drupalSettings.etracker.messages.forEach(function (message) {
      const clickEvent = new et_UserDefinedEvent(
        message.text,
        'Message',
        message.type,
        '',
      );
      _etracker.sendEvent(clickEvent);
    });
  }
})(drupalSettings);
