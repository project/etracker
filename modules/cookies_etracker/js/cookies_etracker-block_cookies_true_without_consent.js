/**
 * @file
 * Defines Javascript behaviors for the cookies_etracker module.
 *
 * This is used when data_block_cookies is TRUE
 * AND knockout_mode is 'block_cookies_true_without_consent'.
 */
(function (Drupal) {
  /**
   * Sets data-block-cookies to "false" once consent was given
   * through COOKiES.
   */
  Drupal.behaviors.cookiesEtrackerBlockCookiesTrueWithoutConsent = {
    // id corresponding to the cookies_service.schema->id.
    id: 'etracker',

    activate(context) {
      const etLoader = document.getElementById('_etLoader');
      if (
        etLoader &&
        etLoader.getAttribute &&
        etLoader.getAttribute('data-block-cookies') === 'true'
      ) {
        // Use cookieful tracking:

        // TODO - There's a bit of race condition danger here
        // as _etracker might have not been initialized yet
        // due to "async" attribute.
        // See https://www.drupal.org/project/etracker/issues/3249453
        // We work around it here as good as possible
        // By using the _etracker and attribute method and deferring the script.
        if (typeof _etracker !== 'undefined') {
          // @see https://www.etracker.com/docs/integration-setup/einstellungen-accounts/etracker-cookies/etracker-cookies-aktivieren/
          _etracker.enableCookies();
        }

        // Additionally represent the state in the data-attribute to prevent
        // confusion and if the script wasn't loaded yet.
        // This will have no effect if the script was already loaded!
        document
          .getElementById('_etLoader')
          .setAttribute('data-block-cookies', 'false');
        if (
          typeof _etracker !== 'undefined' &&
          !_etracker.areCookiesEnabled()
        ) {
          console.log('eTracker cookies should be enabled, but they are not!');
        }
      }
    },

    fallback(context) {
      const etLoader = document.getElementById('_etLoader');
      if (
        etLoader !== undefined &&
        etLoader.getAttribute('data-block-cookies') === 'false'
      ) {
        // Use cookieless tracking:

        // TODO - There's a bit of race condition danger here
        // as _etracker might have not been initialized yet
        // due to "async" attribute.
        // See https://www.drupal.org/project/etracker/issues/3249453
        // We work around it here as good as possible
        // By using the _etracker and attribute method and deferring the script.
        if (typeof _etracker !== 'undefined') {
          // @see https://www.etracker.com/docs/integration-setup/einstellungen-accounts/etracker-cookies/etracker-cookies-aktivieren/
          _etracker.disableCookies();
        }

        // Additionally represent the state in the data-attribute to prevent
        // confusion and if the script wasn't loaded yet.
        // This will have no effect if the script was already loaded!
        document
          .getElementById('_etLoader')
          .setAttribute('data-block-cookies', 'true');
        if (typeof _etracker !== 'undefined' && _etracker.areCookiesEnabled()) {
          console.log(
            'eTracker cookies should be disabled, but they are enabled!',
          );
        }
      }
    },

    attach(context) {
      const self = this;
      document.addEventListener('cookiesjsrUserConsent', function (event) {
        const service =
          typeof event.detail.services === 'object'
            ? event.detail.services
            : {};
        if (typeof service[self.id] !== 'undefined' && service[self.id]) {
          // Consent given. Allow cookies to be used:
          self.activate(context);
        } else {
          // Consent denied / revoked. Disable cookies:
          self.fallback(context);
        }
      });
    },
  };
})(Drupal);
