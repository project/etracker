/**
 * @file
 * Defines Javascript behaviors for the cookies_etracker module.
 *
 * This is used when data_block_cookies is TRUE
 * AND knockout_mode is 'knockout_without_consent'.
 */
(function (Drupal) {
  /**
   * Heals the COOKiES eTracker knockout when user consent was given.
   * Loads the previously knocked-out eTracker scripts.
   */
  Drupal.behaviors.cookiesEtrackerKnockoutWithoutConsent = {
    // id corresponding to the cookies_service.schema->id.
    id: 'etracker',

    activate(context, settings) {
      const scriptIds = ['_etLoader', 'etracker_script'];
      scriptIds.forEach(function (scriptId) {
        const script = document.getElementById(scriptId);
        if (script) {
          const content = script.innerHTML;
          const newScript = document.createElement('script');
          const attributes = Array.from(script.attributes);
          attributes.forEach(function (attribute) {
            const name = attribute.name;
            if (name !== 'type' && name !== 'id') {
              newScript.setAttribute(name, attribute.value);
            }
          });
          // In knockout_mode "knockout_without_consent_block_cookies_false"
          // We have to change data-block-cookies to false before healing
          // the script so that cookies are allowed with consent given.
          // knockout_without_consent_block_cookies_true doesn't need this
          // as the default is always data-block-cookies="true" when this is
          // used.
          if (
            settings.cookies_etracker.knockout_mode ===
            'knockout_without_consent_block_cookies_false'
          ) {
            newScript.setAttribute('data-block-cookies', 'false');
            // When clicking the consent button and loading the script,
            // ensure that it is loaded synchronously to fire the "load"
            // event afterwards.
            // These attributes are only set temporarily for when the
            // consent was given (same page load):
            // TODO: This should work but doesn't, at least in chrome.
            // Not important enough currently as everything works
            // with the next page load after giving consent.
            // newScript.setAttribute("async", "false");
            // newScript.setAttribute("defer", "false");
            // newScript.addEventListener("load", function (event) {
            //   _etracker.enableCookies();
            // });
          }
          newScript.innerHTML = content;
          script.parentNode.replaceChild(newScript, script);

          // Now we have to restore the original ID:
          newScript.setAttribute('id', scriptId);
          // And activate it (as the tests expect):
          newScript.setAttribute('type', 'text/javascript');
        }
      });
    },

    fallback(context, settings) {
      if (typeof _etracker !== 'undefined') {
        // When consent is denied / revoked, disable tracking as if it was knocked out:
        _etracker.disableTrackingForSession();
      }
    },

    attach(context, settings) {
      const self = this;
      document.addEventListener('cookiesjsrUserConsent', function (event) {
        const service =
          typeof event.detail.services === 'object'
            ? event.detail.services
            : {};
        if (typeof service[self.id] !== 'undefined' && service[self.id]) {
          // Consent given. Allow cookies to be used:
          self.activate(context, settings);
        } else {
          // Consent denied / revoked. Disable cookies:
          self.fallback(context, settings);
        }
      });
    },
  };
})(Drupal);
