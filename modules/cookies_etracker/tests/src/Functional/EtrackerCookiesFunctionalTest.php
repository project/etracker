<?php

declare(strict_types=1);

namespace Drupal\Tests\cookies_etracker\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\etracker\Helper\Constants;

/**
 * Tests basic cookies_etracker functionalities.
 *
 * @group cookies_etracker
 */
class EtrackerCookiesFunctionalTest extends BrowserTestBase {
  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'cookies',
    'etracker',
    'cookies_etracker',
    'test_page_test',
    'block',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A test administrator.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A regular authenticated user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The eTracker dummy account key for testing.
   *
   * @var string
   */
  protected $eTrackerAccountKey = 'xxxxxx';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Use the test page as the front page.
    $this->config('system.site')->set('page.front', '/test-page')->save();

    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Create users:
    $this->createAdminRole('administrator', 'administrator');
    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole('administrator');
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the required library exists.
   */
  public function testLibraryExists() {
    $library = \Drupal::service('library.discovery')->getLibraryByName('cookies', 'cookiesjsr');
    // If library does not exist, $library will be false:
    $this->assertNotEquals($library, FALSE);
    // Library path is stored in data:
    if (!file_exists($library['js'][0]['data'])) {
      $this->assertTrue(FALSE);
    }
  }

  /**
   * Tests the script is in header and its type is "text/plain" before consent.
   */
  public function testScriptIsTextPlainBeforeConsentInHeader() {
    $session = $this->assertSession();
    // Set the required settings:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('etracker_scope_script', 'header')->save();
    $etracker_values = [
      'data_respect_dnt' => TRUE,
      'data_block_cookies' => TRUE,
    ];
    $this->config('etracker.settings')->set('etracker_script_settings', $etracker_values)->save();
    $this->config('cookies_etracker.settings')->set('knockout_mode', 'knockout_without_consent_block_cookies_false')->save();
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    // Check script type before consent:
    $this->drupalLogout();
    // Clear caches, otherwise the cached script is returned:
    drupal_flush_all_caches();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $session->elementExists('css', 'head script#_etLoader');
    // Check if the block is placed:
    $session->elementExists('css', '#cookiesjsr');
    // Check if script is "knocked-out":
    $session->elementAttributeContains('css', 'script#_etLoader', 'type', 'application/json');
  }

  /**
   * Tests the script is in footer and its type is "text/plain" before consent.
   */
  public function testScriptIsTextPlainBeforeConsentInFooter() {
    $session = $this->assertSession();
    // Set the required settings:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('etracker_scope_script', 'footer')->save();
    $etracker_values = [
      'data_respect_dnt' => TRUE,
      'data_block_cookies' => TRUE,
    ];
    $this->config('etracker.settings')->set('etracker_script_settings', $etracker_values)->save();
    $this->config('cookies_etracker.settings')->set('knockout_mode', 'knockout_without_consent_block_cookies_false')->save();
    // Place the Cookie UI Block:
    $this->drupalPlaceBlock('cookies_ui_block', ['region' => 'content']);
    // Check script type before consent:
    $this->drupalLogout();
    // Clear caches, otherwise the cached script is returned:
    drupal_flush_all_caches();
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $session->elementExists('css', 'body script#_etLoader');
    // Check if the block is placed:
    $session->elementExists('css', '#cookiesjsr');
    // Check if script is "knocked-out":
    $session->elementAttributeContains('css', 'script#_etLoader', 'type', 'application/json');
  }

}
