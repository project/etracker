<?php

declare(strict_types=1);

namespace Drupal\Tests\cookies_etracker\FunctionalJavascript;

use Drupal\Core\Cache\Cache;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\block\Traits\BlockCreationTrait;
use Drupal\etracker\Helper\Constants;

/**
 * Tests the configuration form.
 *
 * @group cookies_etracker
 * @requires module cookies
 */
class CookieTrackingTest extends WebDriverTestBase {
  use BlockCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'cookies',
    'etracker',
    'cookies_etracker',
    'test_page_test',
    'block',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A test administrator.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A regular authenticated user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The eTracker dummy account key for testing.
   *
   * @var string
   */
  protected $eTrackerAccountKey = 'xxxxxx';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Use the test page as the front page.
    $this->config('system.site')->set('page.front', '/test-page')->save();

    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Create users:
    $this->user = $this->drupalCreateUser();
    $this->createAdminRole('administrator', 'administrator');
    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole('administrator');
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Delete backend caches.
   */
  protected function deleteBackendCaches() {
    foreach (Cache::getBins() as $cache_backend) {
      $cache_backend
        ->deleteAll();
    }
  }

  /**
   * Tests cookie behavior, with default cookie blocking mode.
   */
  public function testCookieEtrackerBlockingModeDefault() {
    $session = $this->assertSession();

    // Set the required settings:
    $etracker_values = [
      'data_respect_dnt' => TRUE,
      'data_block_cookies' => TRUE,
    ];
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    $this->config('etracker.settings')->set('etracker_script_settings', $etracker_values)->save();
    $this->config('cookies_etracker.settings')->set('knockout_mode', 'block_cookies_true_without_consent')->save();
    // Check if the etracker script is loaded and blocks cookies:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'true');
    // Fire consent script:
    $script = "var options = { all: true };
      document.dispatchEvent(new CustomEvent('cookiesjsrSetService', { detail: options }));";
    $this->getSession()->getDriver()->executeScript($script);
    // Check if the script unblocks cookies:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'false');
    $session->cookieExists('cookiesjsr');
  }

  /**
   * Tests cookie behavior, with knocked out consent cookie blocking mode.
   */
  public function testCookieEtrackerBlockingModeKnockedOutCookie() {
    $session = $this->assertSession();

    // Set the required settings:
    $etracker_values = [
      'data_respect_dnt' => TRUE,
      'data_block_cookies' => TRUE,
    ];
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    $this->config('etracker.settings')->set('etracker_script_settings', $etracker_values)->save();
    $this->config('cookies_etracker.settings')->set('knockout_mode', 'knockout_without_consent_block_cookies_false')->save();

    // Delete backend caches:
    $this->deleteBackendCaches();

    // Check if the etracker script is "knocked out":
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementAttributeContains('css', 'script#_etLoader', 'type', 'application/json');

    // Fire consent script:
    $script = "var options = { all: true };
      document.dispatchEvent(new CustomEvent('cookiesjsrSetService', { detail: options }));";
    $this->getSession()->getDriver()->executeScript($script);
    // Check if the script unblocks cookies:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'false');
    $session->elementAttributeContains('css', 'script#_etLoader', 'type', 'text/javascript');
    $session->cookieExists('cookiesjsr');
  }

  /**
   * Tests cookie behavior, with knocked out consent script blocking mode.
   */
  public function testCookieEtrackerBlockingModeKnockedOutScript() {
    $session = $this->assertSession();

    // Set the required settings:
    $etracker_values = [
      'data_respect_dnt' => TRUE,
      'data_block_cookies' => TRUE,
    ];
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    $this->config('etracker.settings')->set('etracker_script_settings', $etracker_values)->save();
    $this->config('cookies_etracker.settings')->set('knockout_mode', 'knockout_without_consent_block_cookies_true')->save();

    // Delete backend caches:
    $this->deleteBackendCaches();

    // Check if the etracker script is "knocked out":
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementAttributeContains('css', 'script#_etLoader', 'type', 'application/json');
    // Fire consent script:
    $script = "var options = { all: true };
      document.dispatchEvent(new CustomEvent('cookiesjsrSetService', { detail: options }));";
    $this->getSession()->getDriver()->executeScript($script);
    // Check if the script unblocks cookies:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementAttributeContains('css', 'script#_etLoader', 'type', 'text/javascript');
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'true');
  }

  /**
   * Tests cookie behavior, with script enabled ignore consent blocking mode.
   */
  public function testCookieEtrackerBlockingModeIgnoreConsentScript() {
    $session = $this->assertSession();

    // Set the required settings:
    $etracker_values = [
      'data_respect_dnt' => TRUE,
      'data_block_cookies' => TRUE,
    ];
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    $this->config('etracker.settings')->set('etracker_script_settings', $etracker_values)->save();
    $this->config('cookies_etracker.settings')->set('knockout_mode', 'knockout_without_consent_block_cookies_true')->save();

    // Delete backend caches:
    $this->deleteBackendCaches();

    // Check if the etracker script is loaded and blocks cookies:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'true');

    // Fire consent script:
    $script = "var options = { all: true };
      document.dispatchEvent(new CustomEvent('cookiesjsrSetService', { detail: options }));";
    $this->getSession()->getDriver()->executeScript($script);
    // Check if the script is still enabled and blocks cookies:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'true');
  }

  /**
   * Tests if the cookies etracker library is not loaded, when ignored by page.
   */
  public function testEtrackerScriptNotLoadedPage() {
    $session = $this->assertSession();
    // Set the required settings:
    $etracker_values = [
      'data_respect_dnt' => TRUE,
      'data_block_cookies' => TRUE,
    ];
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    $this->config('etracker.settings')->set('etracker_script_settings', $etracker_values)->save();
    $this->config('cookies_etracker.settings')->set('knockout_mode', 'knockout_without_consent_block_cookies_false')->save();
    // Check if the etracker script is loaded and blocks cookies:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'true');
    // Check if the correct library is loaded but the incorrect isn't:
    $session->elementExists('css', 'script[src*="knockout_without_consent"]');
    $session->elementNotExists('css', 'script[src*="block_cookies_true_without_consent"]');

    // Now disable etracker on the front page:
    $this->config('etracker.settings')->set('etracker_track_path_mode', 'all_pages')->save();
    $this->config('etracker.settings')->set('etracker_track_paths', '/test-page')->save();

    // Delete backend caches:
    $this->deleteBackendCaches();

    // Check if all libraries are not loaded:
    $this->drupalGet('<front>');
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
    $session->elementNotExists('css', 'script[src*="knockout_without_consent"]');
    $session->elementNotExists('css', 'script[src*="block_cookies_true_without_consent"]');

    // Firing the consent script shouldn't make a difference:
    $script = "var options = { all: true };
      document.dispatchEvent(new CustomEvent('cookiesjsrSetService', { detail: options }));";
    $this->getSession()->getDriver()->executeScript($script);
    // Check if the script is still enabled and blocks cookies:
    $this->drupalGet('<front>');
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
    $session->elementNotExists('css', 'script[src*="knockout_without_consent"]');
    $session->elementNotExists('css', 'script[src*="block_cookies_true_without_consent"]');
  }

  /**
   * Tests if the cookies etracker library is not loaded, when ignored by role.
   */
  public function testEtrackerScriptNotLoadedRole() {
    $session = $this->assertSession();
    // Set the required settings:
    $etracker_values = [
      'data_respect_dnt' => TRUE,
      'data_block_cookies' => TRUE,
    ];
    // Place the Cookie UI Block:
    $this->placeBlock('cookies_ui_block');
    $this->config('etracker.settings')->set('etracker_script_settings', $etracker_values)->save();
    $this->config('cookies_etracker.settings')->set('knockout_mode', 'knockout_without_consent_block_cookies_false')->save();

    // Check if the etracker script is loaded and blocks cookies:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'true');
    // Check if the correct library is loaded but the incorrect isn't:
    $session->elementExists('css', 'script[src*="knockout_without_consent"]');
    $session->elementNotExists('css', 'script[src*="block_cookies_true_without_consent"]');

    // // Now disable etracker on the front page:
    $this->config('etracker.settings')->set('etracker_track_roles_mode', 'all_roles')->save();
    $this->config('etracker.settings')->set('etracker_track_roles', ['administrator'])->save();

    // Delete backend caches:
    $this->deleteBackendCaches();

    // Check if all libraries are not loaded:
    $this->drupalGet('<front>');
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
    $session->elementNotExists('css', 'script[src*="knockout_without_consent"]');
    $session->elementNotExists('css', 'script[src*="block_cookies_true_without_consent"]');

    // Firing the consent script shouldn't make a difference:
    $script = "var options = { all: true };
      document.dispatchEvent(new CustomEvent('cookiesjsrSetService', { detail: options }));";
    $this->getSession()->getDriver()->executeScript($script);
    // Check if the script is still enabled and blocks cookies:
    $this->drupalGet('<front>');
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
    $session->elementNotExists('css', 'script[src*="knockout_without_consent"]');
    $session->elementNotExists('css', 'script[src*="block_cookies_true_without_consent"]');
  }

}
