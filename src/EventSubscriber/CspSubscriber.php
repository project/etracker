<?php

namespace Drupal\etracker\EventSubscriber;

use Drupal\csp\CspEvents;
use Drupal\csp\Event\PolicyAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CspPolicyAlterSubscriber.
 */
class CspSubscriber implements EventSubscriberInterface {

  const TRACKING_DOMAIN = 'www.etracker.de';

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    if (!class_exists(CspEvents::class)) {
      return [];
    }

    return [
      CspEvents::POLICY_ALTER => 'onCspPolicyAlter',
    ];
  }

  /**
   * Alter CSP policy for tracking requests.
   *
   * @param \Drupal\csp\Event\PolicyAlterEvent $alterEvent
   *   The policy alter event.
   */
  public function onCspPolicyAlter(PolicyAlterEvent $alterEvent): void {
    $policy = $alterEvent->getPolicy();

    $policy->fallbackAwareAppendIfEnabled('script-src', [self::TRACKING_DOMAIN]);
    $policy->fallbackAwareAppendIfEnabled('script-src-elem', [self::TRACKING_DOMAIN]);
  }

}
