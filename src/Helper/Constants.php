<?php

namespace Drupal\etracker\Helper;

/**
 * Common used constants.
 */
class Constants {
  public const ETRACKER_JS_URL = '//code.etracker.com/code/e.js';

  public const ETRACKER_LIBRARY_NAME = 'etracker.js';
  public const ETRACKER_FULL_LIBRARY_NAME = 'etracker/' . self::ETRACKER_LIBRARY_NAME;

  public const ETRACKER_SETTINGS_CONFIG_NAME = 'etracker.settings';

  public const ETRACKER_SCOPE_SCRIPT_HEADER = 'header';
  public const ETRACKER_SCOPE_SCRIPT_FOOTER = 'footer';

  public const ETRACKER_TRACK_PATHS_MODE_ALL = 'all_pages';
  public const ETRACKER_TRACK_PATHS_MODE_LISTED = 'listed_pages';

  public const ETRACKER_TRACK_ROLES_MODE_ALL = 'all_roles';
  public const ETRACKER_TRACK_ROLES_MODE_LISTED = 'listed_roles';

  public const ETRACKER_TRACK_USER_NO_CUSTOMIZATION = 'no_customization';
  public const ETRACKER_TRACK_USER_TRACKING_ON = 'tracking_on';
  public const ETRACKER_TRACK_USER_TRACKING_OFF = 'tracking_off';

  public const ETRACKER_MODE_BREADCRUMB_ON = 'breadcrumb_on';
  public const ETRACKER_MODE_BREADCRUMB_OFF = 'breadcrumb_off';
  public const ETRACKER_MODE_BREADCRUMB_ON_EXCLUDE_HOME = 'breadcrumb_exclude_home';

  public const ETRACKER_TRACK_PATHS = "admin\nadmin/*\nbatch\nnode/add*\nnode/*/*\nuser/*/*";

}
