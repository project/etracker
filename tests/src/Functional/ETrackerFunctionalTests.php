<?php

declare(strict_types=1);

namespace Drupal\Tests\etracker\Functional;

use Drupal\Tests\BrowserTestBase;

// Include etracker constants file:
use Drupal\etracker\Helper\Constants;

/**
 * This class provides methods for testing the etracker browser functionalities.
 *
 * @group etracker
 */
class ETrackerFunctionalTests extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'etracker',
    'test_page_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * An admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminuser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The eTracker dummy account key for testing.
   *
   * @var string
   */
  protected $eTrackerAccountKey = 'xxxxxx';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Use the test page as the front page:
    $this->config('system.site')->set('page.front', '/test-page')->save();
    // Create an user:
    $this->user = $this->drupalCreateUser([
      'administer etracker',
      'opt-in or out of etracker tracking',
      'access content',
    ]);

    // Create an admin user:
    $this->adminuser = $this->drupalCreateUser(['administer etracker']);
    $this->adminuser->addRole($this->createAdminRole('administrator', 'administrator'));
    $this->adminuser->save();

    // Login as regular authenticated user by default:
    $this->drupalLogin($this->user);
  }

  /**
   * Tests if the form is unaccessible as an anonymous user.
   */
  public function testEtrackerAccessFormAsAnonymous() {
    $this->drupalLogout();
    $session = $this->assertSession();

    // Go to settings and see if the anonymous user has no access:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(403);
  }

  /**
   * Tests if the form is accessible as an admin user.
   */
  public function testEtrackerAccessFormAsAdmin() {
    $session = $this->assertSession();
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);

    // Go to settings and see if an admin has access:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if setting the account key via form works as intended.
   */
  public function testEtrackerFormAccountKey() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Go to settings and see if they exist:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Fill the account key:
    $page->fillField('edit-account-key', $this->eTrackerAccountKey);
    $page->pressButton('edit-submit');

    // Check if the configuration was saved:
    $session->statusCodeEquals(200);
    $session->pageTextContains('The configuration options have been saved.');

    // Check if the account key is set in the config:
    $this->assertEquals($this->eTrackerAccountKey, \Drupal::config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->get('account_key'));
  }

  /**
   * Tests if the etracker script exists in the header.
   */
  public function testEtrackerInHeaderExists() {
    $session = $this->assertSession();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // See if account key is the same as in the config:
    $this->assertEquals($this->eTrackerAccountKey, $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->get('account_key'));
    // Set the script header configuration:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('etracker_scope_script', 'header')->save();

    // Check script is in head:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'head script#_etLoader');
    $session->elementExists('css', 'head script#etracker_script');

    // By default, anonymous users should have the same result:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'head script#_etLoader');
    $session->elementExists('css', 'head script#etracker_script');
  }

  /**
   * Tests if the etracker script exists in the footer.
   */
  public function testEtrackerInFooterExists() {
    $session = $this->assertSession();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('etracker_scope_script', 'footer')->save();

    // Check script is in body:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'body script#_etLoader');
    $session->elementExists('css', 'body script#etracker_script');

    // By default, anonymous users should see the same script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->elementExists('css', 'body script#_etLoader');
    $session->elementExists('css', 'body script#etracker_script');
  }

  /**
   * Tests if cookie-less tracking is enabled.
   */
  public function testDataBlockCookieValueEnabled() {
    $session = $this->assertSession();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();

    // Go to front page and see if the value is set in the script:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'true');

    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-block-cookies', 'true');
  }

  /**
   * Tests if Do-Not_track is enabled.
   */
  public function testDataRespectDntValueEnabled() {
    $session = $this->assertSession();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();

    // Go to front page and see if the value is set in the script:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-respect-dnt', 'true');

    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', 'script#_etLoader', 'data-respect-dnt', 'true');
  }

  /**
   * Tests if the script is not loaded on a specific path with a slash.
   */
  public function testExclusionByPathWithSlashUrl() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Don't track on test-page:
    $page->fillField('edit-etracker-track-path-mode-all-pages', 'all_pages');
    $page->fillField('edit-etracker-track-paths', '/test-page');
    $page->pressButton('edit-submit');

    // See if script won't load for our authenticated user:
    $this->drupalGet('/test-page');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');

    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('/test-page');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is loaded on a specific path with a slash.
   */
  public function testInclusionByPathWithSlashUrl() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Track on test-page:
    $page->fillField('edit-etracker-track-path-mode-all-pages', 'listed_pages');
    $page->fillField('edit-etracker-track-paths', '/test-page');
    $page->pressButton('edit-submit');

    // See if script will load for our authenticated user:
    $this->drupalGet('/test-page');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // By default, anonymous users should have the same result.
    $this->drupalLogout();
    $this->drupalGet('/test-page');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
  }

  /**
   * Tests if script is not loaded on a specific path, written without a slash.
   */
  public function testExclusionByPathWithoutSlashUrl() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Don't track on test-page:
    $page->fillField('edit-etracker-track-path-mode-all-pages', 'all_pages');
    $page->fillField('edit-etracker-track-paths', 'test-page');
    $page->pressButton('edit-submit');

    // See if script will load for our authenticated user, since the path
    // is written without the slash:
    $this->drupalGet('/test-page');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // By default, anonymous users should have the same result:
    $this->drupalLogout();
    $this->drupalGet('/test-page');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
  }

  /**
   * Tests if script is loaded on a specific path written without a slash.
   */
  public function testInclusionByPathWithoutSlashUrl() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Check if tracked on test-page:
    $page->fillField('edit-etracker-track-path-mode-all-pages', 'listed_pages');
    $page->fillField('edit-etracker-track-paths', 'test-page');
    $page->pressButton('edit-submit');

    // See if script will load for our authenticated user, since the path
    // is written without the slash:
    $this->drupalGet('/test-page');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // By default, anonymous users should have the same result:
    $this->drupalLogout();
    $this->drupalGet('/test-page');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is not loaded on an authenticated user.
   */
  public function testExclusionByRoleAuthenticatedTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Track all roles except authenticated user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'all_roles');
    $page->fillField('edit-etracker-track-roles-authenticated', 'authenticated');
    $page->pressButton('edit-submit');

    // See if script won't load for our authenticated user:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');

    // Anonymous users should still "see" the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is loaded on an authenticated user.
   */
  public function testInclusionByRoleAuthenticatedTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Only track authenticated user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'listed_roles');
    $page->fillField('edit-etracker-track-roles-authenticated', 'authenticated');
    $page->pressButton('edit-submit');

    // See if script will load for our authenticated user:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // Anonymous users shouldn't see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is not loaded on an anonymous user.
   */
  public function testExclusionByRoleAnonymousTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Track all roles except anonymous user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'all_roles');
    $page->fillField('edit-etracker-track-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');

    // Logout and see if script won't load for a an anonymous user:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');

    // Authenticated users should still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is loaded on an anonymous user.
   */
  public function testInclusionByRoleAnonymousTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);

    // Only track anonymous user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'listed_roles');
    $page->fillField('edit-etracker-track-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');

    // Logout and see if script will load for an anonymous user:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // Authenticated users shouldn't see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is not loaded on an admin user.
   */
  public function testExclusionByRoleAdminTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Track all roles except admin user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'all_roles');
    $page->fillField('edit-etracker-track-roles-administrator', 'administrator');
    $page->pressButton('edit-submit');

    // Check if anonymous users still see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // Check if authenticated users still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);

    // Admin user shouldn't see the script:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is loaded on an admin user.
   */
  public function testInclusionByRoleAdminTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Only track admin user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'listed_roles');
    $page->fillField('edit-etracker-track-roles-administrator', 'administrator');
    $page->pressButton('edit-submit');
    $this->drupalLogout();

    // Anonymous users shouldn't see the script:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');

    // Authenticated users should still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');

    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);

    // Admin user should see the script:
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is not loaded on multiple users.
   */
  public function testExclusionByRoleMultipleRoleTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Track all roles except authenticated and anonymous user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'all_roles');
    $page->fillField('edit-etracker-track-roles-authenticated', 'authenticated');
    $page->fillField('edit-etracker-track-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');
    // See if script won't load for our authenticated user:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');

    // Anonymous users also shouldn't see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');
  }

  /**
   * Tests if the script is loaded on multiple users.
   */
  public function testInclusionByRoledMultipleRoleTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Only track authenticated and anonymous user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'listed_roles');
    $page->fillField('edit-etracker-track-roles-authenticated', 'authenticated');
    $page->fillField('edit-etracker-track-roles-anonymous', 'anonymous');
    $page->pressButton('edit-submit');

    // See if script will load for our authenticated user:
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // Anonymous users also should see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
  }

  /**
   * Tests if script is loaded on an admin user, inherited from authenticated.
   */
  public function testInclusionByRoleAuthenticatedInheritTest() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Login as adminuser:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    // Set account key:
    $this->config(Constants::ETRACKER_SETTINGS_CONFIG_NAME)->set('account_key', $this->eTrackerAccountKey)->save();
    // Go to etracker settings page:
    $this->drupalGet('/admin/config/system/etracker');
    $session->statusCodeEquals(200);
    // Only track admin user:
    $page->fillField('edit-etracker-track-roles-mode-all-roles', 'listed_roles');
    $page->fillField('edit-etracker-track-roles-authenticated', 'authenticated');
    $page->pressButton('edit-submit');

    // Anonymous users shouldn't see the script:
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementNotExists('css', 'script#_etLoader');
    $session->elementNotExists('css', 'script#etracker_script');

    // Authenticated users still see the script:
    $this->drupalLogin($this->user);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');

    // Login as adminuser, shouldn't see the script:
    $this->drupalLogout();
    $this->drupalLogin($this->adminuser);
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    $session->elementExists('css', 'script#_etLoader');
    $session->elementExists('css', 'script#etracker_script');
  }

}
